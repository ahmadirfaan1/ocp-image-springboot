package com.example.testopenshift.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/hello")
@RestController
public class HelloWorld {

    @GetMapping()
    public String helloWorld() {
        return "Hai Guys Hello Aku Irfaan Ganteng";
    }
}
